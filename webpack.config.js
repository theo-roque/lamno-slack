const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const InterpolateHtmlPlugin = require("react-dev-utils/InterpolateHtmlPlugin");
const InlineChunkHtmlPlugin = require("react-dev-utils/InlineChunkHtmlPlugin");
const WatchMissingNodeModulesPlugin = require("react-dev-utils/WatchMissingNodeModulesPlugin");
const paths = require("./config/paths");

module.exports = {
    entry: path.resolve(__dirname, "src") + "/index.js",
    output: {
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|mjs)$/,
                exclude: [/@babel(?:\/|\\{1,2})runtime/, /node_modules/],
                loader: "babel-loader",
                options: {
                    babelrc: true,
                    configFile: false,
                    compact: false,
                    presets: [
                        [
                            "babel-preset-react-app/dependencies",
                            { helpers: true }
                        ],
                        "@babel/preset-env",
                        "@babel/preset-react"
                    ],
                    cacheDirectory: true,
                    cacheCompression: false,
                    sourceMaps: false,
                    plugins: ["@babel/plugin-proposal-class-properties"]
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.(jpe?g|gif|png|svg)$/i,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 10000
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        alias: paths.appAlias,
        extensions: [".js", ".jsx"]
    },
    devServer: {
        contentBase: "./public",
        hot: true,
        historyApiFallback: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./public/index.html"
        }),
        new InterpolateHtmlPlugin(HtmlWebpackPlugin, {
            PUBLIC_URL: ""
        }),
        new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/runtime/]),
        new WatchMissingNodeModulesPlugin(path.resolve("node_modules"))
    ]
};

This project is created for sole purpose of examination.

You may access the demo at https://theo-lamno-slack.netlify.com

# Slack Workspace and User sign-in for Lamno

### Enhancements needed:

-   Create an AuthManager thunk/actions to handle all the store in one place (workspace and user)
-   Thunks and Reducers are done separately to show how the developer structures components especially on big projects
-   Loading / Submitting State is not done
-   Button component doesn't accept icons

import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { persistStore } from "redux-persist";
import { ConnectedRouter } from "connected-react-router";
import configureStore, { history } from "lscore/store";
import RouterManager from "lsutils/RouterManager";
import "lsassets/scss/base/_base.scss";
import "lsassets/scss/base/_font.scss";
import "lsassets/scss/base/_typography.scss";
import * as serviceWorker from "./serviceWorker";

const store = configureStore({});
const persistor = persistStore(store);
ReactDOM.render(
    <PersistGate persistor={persistor}>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Router history={history}>
                    <RouterManager />
                </Router>
            </ConnectedRouter>
        </Provider>
    </PersistGate>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

import { required, email } from "redux-form-validators";

/**
 * valueValidation prepares validators for field
 * @param {Array} validators Description of field validation
 * @return {Array} Returns array of validation functions for field
 */
const valueValidation = validators => {
    const preparedValidators = [];
    //  Skip validators maps if they are empty
    if (!validators) {
        return false;
    }
    //  Fill validatiors with arguments
    validators.map(validation => {
        const { validator, args } = validation;
        if (validator === "required") {
            preparedValidators.push(required(args));
        } else if (validator === "email") {
            preparedValidators.push(email(args));
        }
        return true;
    });
    return preparedValidators;
};

export default valueValidation;

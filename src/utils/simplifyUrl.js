/**
 * Formatting a URL without https and trailing slash
 * @param string {url}
 * @returns {string}
 */
export default function simplifyUrl(url) {
    return url.replace(/^\/\/$|^.*?:(\/\/)?/, "").replace(/\/$/, "");
}

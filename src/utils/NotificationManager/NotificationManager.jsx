import React from "react";
import PropTypes from "prop-types";
import Notification from "lscomponents/Notification";

/**
 * Notification Manager
 * @param code
 * @param message
 * @returns {*}
 * @constructor
 */
const NotificationManager = ({ code }) => {
    switch (code) {
        case "team_not_found":
            return (
                <Notification
                    type="error"
                    title="We couldn’t find your workspace."
                    description="If you can’t remember your workspace’s address, we can"
                >
                    <a href="/find">send you a reminder.</a>
                </Notification>
            );
        case "user_not_found":
            return (
                <Notification
                    type="error"
                    description="Sorry, you entered an incorrect email address or password."
                />
            );
        case "incorrect_password":
            return (
                <Notification
                    type="error"
                    description="Sorry, you entered an incorrect password."
                />
            );
        case "ratelimited":
            return (
                <Notification
                    type="error"
                    title="Too many login failures!"
                    description="Apologies! Too many incorrect codes were entered in too short a time. Please wait a few moments before trying again."
                />
            );
        case "internal_error":
            return (
                <Notification
                    type="error"
                    description="Something went wrong."
                />
            );
        default:
            return (
                <Notification
                    type="error"
                    description="Something went wrong."
                />
            );
    }
};

NotificationManager.defaultProps = {
    code: null
};

NotificationManager.propTypes = {
    code: PropTypes.string
};

export default NotificationManager;

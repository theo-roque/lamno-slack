export const actions = {
    SET_PAGE_NOTIFICATION: "SET_PAGE_NOTIFICATION",
    REMOVE_PAGE_NOTIFICATION: "REMOVE_PAGE_NOTIFICATION"
};

export const initialState = {
    pageNotification: null
};

export default (state = initialState, { type, response }) => {
    switch (type) {
        case actions.SET_PAGE_NOTIFICATION: {
            return {
                ...state,
                pageNotification: response
            };
        }
        case actions.REMOVE_PAGE_NOTIFICATION: {
            return {
                ...state,
                pageNotification: null
            };
        }
        default:
            return state;
    }
};

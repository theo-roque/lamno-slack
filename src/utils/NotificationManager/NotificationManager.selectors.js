import { createSelector } from "reselect";

export const getPageNotification = state =>
    state.notificationManager.pageNotification;

export default createSelector([getPageNotification]);

import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import SignIn from "lsscreens/SignIn";
import WorkspaceSignIn from "lsscreens/WorkspaceSignIn";
import Client from "lsscreens/Client";

const RouterManager = () => {
    return (
        <Switch>
            <Route key={1} path="/" exact component={SignIn} />
            <Route key={2} path="/:domain" exact component={WorkspaceSignIn} />
            <Route key={3} path="/:domain/:team/:id" exact component={Client} />
            {/* <Route component={Error404} /> */}
        </Switch>
    );
};

export default withRouter(RouterManager);

import { combineReducers } from "redux";

//  Importing reducers
import { reducer as form } from "redux-form";
import { connectRouter } from "connected-react-router";
import notificationManager from "lsutils/NotificationManager/NotificationManager.reducer";
import signIn from "lsscreens/SignIn/SignIn.reducer";
import workspaceSignIn from "lsscreens/WorkspaceSignIn/WorkspaceSignIn.reducer";
import client from "lsscreens/Client/Client.reducer";

//  Combining all existing reducers
const createAppReducer = history =>
    combineReducers({
        notificationManager,
        signIn,
        workspaceSignIn,
        client,
        router: connectRouter(history),
        form
    });

const createRootReducer = history => (state, action) => {
    let newState = { ...state };
    if (action.type === "RESET_STATE") {
        newState = undefined;
    } else if (action.type === "START_SIGNOUT") {
        newState = {
            ...state,
            workspaceSignIn: {
                ...state.workspaceSignIn,
                user: {}
            }
        };
    }

    return createAppReducer(history)(newState, action);
};

export default createRootReducer;

import React from "react";
import PropTypes from "prop-types";
import "./Container.scss";

const Container = ({ children }) => {
    return <div className="container">{children}</div>;
};

Container.defaultProps = {
    children: null
};

Container.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ])
};

export default Container;

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import NotificationManager from "lsutils/NotificationManager";
import Navbar from "lscomponents/Navbar";
import Container from "lscomponents/Container";
import Box from "lscomponents/Box";
import { getPageNotification } from "lsutils/NotificationManager/NotificationManager.selectors";
import "./Layout.scss";

const Layout = ({ children, pageNotification }) => {
    return (
        <>
            <Navbar />
            <main className="main">
                <Container>
                    {pageNotification && (
                        <NotificationManager code={pageNotification} />
                    )}
                    <Box>{children}</Box>
                </Container>
            </main>
        </>
    );
};

Layout.defaultProps = {
    children: null,
    pageNotification: null
};

Layout.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]),
    pageNotification: PropTypes.oneOfType([PropTypes.object, PropTypes.string])
};

const mapStateToProps = state => ({
    pageNotification: getPageNotification(state)
});

export default connect(mapStateToProps)(Layout);

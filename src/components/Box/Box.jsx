import React from "react";
import PropTypes from "prop-types";
import "./Box.scss";

const Box = ({ children }) => {
    return <div className="box">{children}</div>;
};

Box.defaultProps = {
    children: null
};

Box.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ])
};

export default Box;

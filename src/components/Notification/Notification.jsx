import React from "react";
import PropTypes from "prop-types";
import { TiWarningOutline, TiInputChecked } from "react-icons/ti";
import "./Notification.scss";

const Notification = ({ type, title, description, children }) => {
    return (
        <div className="notification">
            {type === "error" ? (
                <TiWarningOutline className="notification__icon" />
            ) : (
                <TiInputChecked className="notification__icon" />
            )}
            {title && (
                <>
                    <span className="notification__title">{title}</span>
                    <br />
                </>
            )}
            <span className="notification__description">
                {description} {children}
            </span>
        </div>
    );
};

Notification.defaultProps = {
    type: "",
    title: "",
    description: "",
    children: null
};

Notification.propTypes = {
    type: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ])
};

export default Notification;

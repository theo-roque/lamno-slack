import React from "react";
import PropTypes from "prop-types";
import "./Input.scss";

const Input = ({ input, type, placeholder, name, className, meta }) => {
    const { error, dirty } = meta;
    return (
        <input
            {...input}
            className={`input ${
                dirty && error ? "input--error" : ""
            } ${className}`}
            type={type}
            placeholder={placeholder}
            name={name}
        />
    );
};

Input.defaultProps = {
    type: "",
    placeholder: "",
    name: "",
    className: ""
};

Input.propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
    name: PropTypes.string,
    className: PropTypes.string,
    input: PropTypes.oneOfType([PropTypes.object]).isRequired,
    meta: PropTypes.oneOfType([PropTypes.object]).isRequired
};

export default Input;

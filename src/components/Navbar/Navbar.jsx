import React from "react";
import "./Navbar.scss";

const Navbar = () => {
    return (
        <nav className="header">
            <a href="/" className="logo" aria-label="Slack homepage"></a>
        </nav>
    );
};

export default Navbar;

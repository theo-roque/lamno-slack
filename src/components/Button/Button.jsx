import React, { useRef } from "react";
import PropTypes from "prop-types";
// import icons from "./iconManifest";
import "./Button.scss";

/**
 *
 * @param {*} props
 */
const Button = ({
    classNameArg,
    disabled,
    loading,
    label,
    iconName,
    submit,
    onClick,
    form
}) => {
    const buttonEl = useRef(null);

    /**
     *
     * @returns {*}
     */
    const handleIcon = () => {
        if (iconName && iconName !== "") {
            return (
                <>
                    {loading && "Loading"}
                    {label !== "" ? (
                        <span className="button__label">{label}</span>
                    ) : (
                        label
                    )}
                </>
            );
        }

        if (loading && (!iconName || iconName === "")) {
            return (
                <>
                    Loading <span className="button__label">{label}</span>
                </>
            );
        }

        return label;
    };

    /**
     *
     * @returns {string|*}
     */
    const handleVersions = () => {
        let className = `button ${classNameArg}`;

        if (disabled) {
            className = `${className} button--disabled`;
        } else {
            className = className.replace(" button--disabled", "");
        }

        if (loading) {
            className = `${className} button--loading`;
        } else {
            className = className.replace(" button--loading", "");
        }

        return className;
    };

    let ariaLabel = "";
    if (label) {
        if (typeof label === "string") {
            ariaLabel = label;
        } else {
            ariaLabel = label.props.defaultMessage;
        }
    }

    if (submit) {
        return (
            <button
                type="submit"
                aria-label={ariaLabel}
                className={handleVersions()}
                disabled={disabled}
                ref={buttonEl}
                onClick={onClick}
                form={form}
            >
                {handleIcon()}
            </button>
        );
    }

    return (
        <button
            type="button"
            aria-label={ariaLabel}
            className={handleVersions()}
            disabled={disabled}
            ref={buttonEl}
            onClick={onClick}
            form={form}
        >
            {handleIcon()}
        </button>
    );
};

Button.defaultProps = {
    classNameArg: "",
    label: "Button",
    iconName: "",
    disabled: false,
    loading: false,
    submit: false,
    onClick: null,
    target: "_self",
    form: null
};

Button.propTypes = {
    classNameArg: PropTypes.string,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    iconName: PropTypes.string,
    form: PropTypes.string,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    submit: PropTypes.bool,
    onClick: PropTypes.func,
    target: PropTypes.string
};

export default Button;

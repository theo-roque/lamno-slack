import { applyMiddleware, createStore, compose } from "redux";
import thunk from "redux-thunk";
import combinedReducers from "lscore/reducers";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

/**
|--------------------------------------------------
| Put reducers to local storage
| Do not store form to local storage
|--------------------------------------------------
*/

export const history = createBrowserHistory();

export default function configureStore(preloadedState) {
    const reducer = persistReducer(
        { key: "state", storage },
        combinedReducers(history)
    );
    const store = createStore(
        reducer, // root reducer with router state
        preloadedState,
        compose(
            applyMiddleware(
                routerMiddleware(history), // for dispatching history actions
                thunk
            )
        )
    );

    return store;
}

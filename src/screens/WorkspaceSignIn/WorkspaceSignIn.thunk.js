import restClient from "lscore/api/restClient";
import {
    showNotification,
    removeNotification
} from "lsutils/NotificationManager/NotificationManager.thunk";
import { actions as workspaceSignInActions } from "lsscreens/WorkspaceSignIn/WorkspaceSignIn.reducer";
import { push } from "react-router-redux";

export const signInToWorkspace = ({ email, password, team }, domain) => {
    return dispatch => {
        dispatch(signInStarted());

        // Using unsecure application/x-www-form-urlencoded due to CORS policy issue on localhost
        restClient
            .post("auth.signin", null, {
                params: {
                    email,
                    password,
                    team
                }
            })
            .then(res => {
                if (res.data.ok) {
                    dispatch(signInSuccess(res.data));
                    dispatch(removeNotification());
                    dispatch(
                        push(`/${domain}/${res.data.team}/${res.data.user}`)
                    );
                } else {
                    dispatch(signInFailed({ code: res.data.error }));
                    dispatch(showNotification(res.data.error));
                }
            })
            .catch(err => {
                dispatch(signInFailed(err.response.data));
                dispatch(showNotification(err.response.data));
            });
    };
};

const signInStarted = () => ({
    type: workspaceSignInActions.START_WORKSPACE_SIGNIN
});

const signInSuccess = res => ({
    type: workspaceSignInActions.WORKSPACE_SIGNIN_SUCCESSFUL,
    response: {
        ...res
    }
});

const signInFailed = error => ({
    type: workspaceSignInActions.WORKSPACE_SIGNIN_FAILED,
    response: error
});

import { createSelector } from "reselect";

// Check if team and id URL params matches the one's in store
export const getUser = (state, team, id) =>
    state.workspaceSignIn.user.team === team &&
    state.workspaceSignIn.user.user === id &&
    state.workspaceSignIn.user;

export default createSelector([getUser]);

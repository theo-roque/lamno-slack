import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
import Layout from "lscomponents/Layout";
import Button from "lscomponents/Button";
import Input from "lscomponents/Input";
import valueValidation from "lsutils/valueValidation";
import simplifyUrl from "lsutils/simplifyUrl";
import { signInToWorkspace } from "./WorkspaceSignIn.thunk";
import { getWorkspace } from "lsscreens/SignIn/SignIn.selectors";
import "./WorkspaceSignIn.scss";

let WorkspaceSignIn = ({
    handleSubmit,
    onSubmit,
    match,
    workspace,
    invalid,
    submitting,
    pristine
}) => {
    return (
        <Layout>
            {workspace ? (
                <div className="workspace-signin">
                    <div className="workspace-signin__title">
                        <h1>
                            Sign in to{" "}
                            <span className="workspace-signin__domain">
                                {match.params.domain}
                            </span>
                        </h1>
                        <p>{simplifyUrl(workspace.url)}</p>
                    </div>

                    <div className="workspace-signin__wrapper">
                        <p className="workspace-signin__instruction">
                            Enter your <strong>email address</strong> and{" "}
                            <strong>password</strong>.
                        </p>
                        <form
                            onSubmit={handleSubmit(formData =>
                                onSubmit(formData, workspace.team_id)
                            )}
                            className="workspace-signin__form"
                            form="WorkspaceSignInForm"
                        >
                            <Field
                                name="email"
                                component={Input}
                                type="email"
                                placeholder="you@example.com"
                                validate={valueValidation([
                                    { validator: "required" },
                                    { validator: "email" }
                                ])}
                            />
                            <Field
                                name="password"
                                component={Input}
                                type="password"
                                placeholder="password"
                                validate={valueValidation([
                                    { validator: "required" }
                                ])}
                            />
                            <Button
                                submit
                                label="Sign in"
                                disabled={invalid || pristine}
                                loading={submitting}
                            />
                        </form>
                    </div>
                </div>
            ) : (
                <h1>
                    Looks like you&#39;re lost.
                    <br />
                    Go back to <a href="/">sign in</a>.
                </h1>
            )}
        </Layout>
    );
};

WorkspaceSignIn.defaultProps = {
    onSubmit: () => {},
    match: {},
    workspace: {}
};

WorkspaceSignIn.propTypes = {
    onSubmit: PropTypes.func,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    match: PropTypes.oneOfType([PropTypes.object]),
    workspace: PropTypes.oneOfType([PropTypes.object])
};

WorkspaceSignIn = reduxForm({
    form: "WorkspaceSignInForm",
    touchOnBlur: false
})(WorkspaceSignIn);

const mapStateToProps = (state, ownProps) => ({
    workspace: getWorkspace(state, ownProps.match.params.domain)
});

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onSubmit: (formData, team) => {
            dispatch(
                signInToWorkspace(
                    { ...formData, team },
                    ownProps.match.params.domain
                )
            );
        }
    };
};

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(WorkspaceSignIn)
);

export const actions = {
    START_WORKSPACE_SIGNIN: "START_WORKSPACE_SIGNIN",
    WORKSPACE_SIGNIN_SUCCESSFUL: "WORKSPACE_SIGNIN_SUCCESSFUL",
    WORKSPACE_SIGNIN_FAILED: "WORKSPACE_SIGNIN_FAILED"
};

export const initialState = {
    error: null,
    user: {}
};

export default (state = initialState, { type, response }) => {
    switch (type) {
        case actions.WORKSPACE_SIGNIN_SUCCESSFUL: {
            return {
                ...state,
                user: {
                    ...response
                }
            };
        }
        case actions.WORKSPACE_SIGNIN_FAILED: {
            return {
                ...state,
                error: response,
                user: {}
            };
        }
        default:
            return state;
    }
};

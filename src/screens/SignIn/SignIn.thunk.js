import restClient from "lscore/api/restClient";
import {
    showNotification,
    removeNotification
} from "lsutils/NotificationManager/NotificationManager.thunk";
import { actions as signInActions } from "./SignIn.reducer";
import { push } from "react-router-redux";

export const signIn = formData => {
    return dispatch => {
        dispatch(signInStarted());

        restClient
            .post("auth.findTeam", null, {
                params: {
                    ...formData
                }
            })
            .then(res => {
                if (res.data.ok) {
                    dispatch(signInSuccess(res.data));
                    dispatch(removeNotification());
                    dispatch(push(`/${formData.domain}`));
                } else {
                    dispatch(signInFailed({ code: res.data.error }));
                    dispatch(showNotification(res.data.error));
                }
            })
            .catch(err => {
                dispatch(signInFailed(err.response.data));
                dispatch(showNotification(err.response.data));
            });
    };
};

const signInStarted = () => ({
    type: signInActions.START_SIGNIN
});

const signInSuccess = response => ({
    type: signInActions.SIGNIN_SUCCESSFUL,
    response
});

const signInFailed = error => ({
    type: signInActions.SIGNIN_FAILED,
    response: error
});

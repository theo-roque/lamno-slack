import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Layout from "lscomponents/Layout";
import Button from "lscomponents/Button";
import { Field, reduxForm } from "redux-form";
import { reset } from "lsscreens/Client/Client.thunk";
import { signIn } from "./SignIn.thunk";
import "./SignIn.scss";

let SignIn = ({ handleSubmit, onSubmit, dispatch }) => {
    useEffect(() => {
        // Reset everytime the home page is accessed
        dispatch(reset());
    }, []);

    return (
        <Layout>
            <div className="signin">
                <h1>Sign in to your workplace</h1>
                <div className="signin__wrapper">
                    <p>
                        Enter your workspace&#39;s <strong>Slack URL</strong>
                    </p>
                    <form
                        onSubmit={handleSubmit(onSubmit)}
                        className="signin__form"
                    >
                        <div className="signin__form__domain-input">
                            <Field
                                name="domain"
                                component="input"
                                type="text"
                                className="input input--right signin__form__domain-input__input"
                                placeholder="your-workspace-url"
                            />
                            <span className="signin__form__domain-input__placeholder">
                                .slack.com
                            </span>
                        </div>

                        <Button
                            submit
                            label="Continue"
                            iconName="TiArrowRight"
                        />
                    </form>
                    <p>
                        Don&#39;t know your workspace URL?{" "}
                        <a href="/find">Find your workspace</a>
                    </p>
                </div>
            </div>
        </Layout>
    );
};

SignIn.defaultProps = {
    onSubmit: () => {}
};

SignIn.propTypes = {
    onSubmit: PropTypes.func,
    handleSubmit: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired
};

SignIn = reduxForm({
    form: "SignInForm"
})(SignIn);

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: formData => {
            dispatch(signIn(formData));
        }
    };
};

export default connect(null, mapDispatchToProps)(SignIn);

import { createSelector } from "reselect";

// Check if domain URL params matches the one's in store
export const getWorkspace = (state, domain) =>
    state.signIn.workspace.url.includes(domain) && state.signIn.workspace;

export default createSelector([getWorkspace]);

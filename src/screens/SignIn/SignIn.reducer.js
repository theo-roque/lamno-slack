export const actions = {
    START_SIGNIN: "START_SIGNIN",
    SIGNIN_SUCCESSFUL: "SIGNIN_SUCCESSFUL",
    SIGNIN_FAILED: "SIGNIN_FAILED"
};

export const initialState = {
    error: null,
    workspace: {}
};

export default (state = initialState, { type, response }) => {
    switch (type) {
        case actions.SIGNIN_SUCCESSFUL: {
            return {
                ...state,
                workspace: {
                    ...response
                },
                error: null
            };
        }
        case actions.SIGNIN_FAILED: {
            return {
                ...state,
                error: response,
                workspace: {}
            };
        }
        default:
            return state;
    }
};

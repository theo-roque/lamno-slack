import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getUser } from "lsscreens/WorkspaceSignIn/WorkspaceSignIn.selectors";
import { getWorkspace } from "lsscreens/SignIn/SignIn.selectors";
import { signOut } from "./Client.thunk";
import Layout from "lscomponents/Layout";
import Button from "lscomponents/Button";
import simplifyUrl from "lsutils/simplifyUrl";

const Client = ({ user, workspace, dispatch, match }) => {
    return (
        <Layout>
            {user && workspace ? (
                <>
                    <h1>
                        You are signed in to
                        <br />
                        <span className="emphasis">
                            {simplifyUrl(workspace.url)}
                        </span>{" "}
                        as
                        <br />
                        <span className="emphasis">{user.user_email}</span>
                    </h1>
                    <Button
                        label="Sign out"
                        onClick={() => dispatch(signOut(match.params.domain))}
                    />
                </>
            ) : (
                <>
                    <h1>
                        Looks like you&#39;re on the wrong workspace or user.
                        <br />
                    </h1>
                    Go back to <a href="/">sign in</a>.
                </>
            )}
        </Layout>
    );
};

const mapStateToProps = (state, ownProps) => ({
    user: getUser(state, ownProps.match.params.team, ownProps.match.params.id),
    workspace: getWorkspace(state, ownProps.match.params.domain)
});

Client.defaultProps = {
    match: {},
    user: {},
    workspace: {}
};

Client.propTypes = {
    match: PropTypes.oneOfType([PropTypes.object]),
    user: PropTypes.oneOfType([PropTypes.object]),
    workspace: PropTypes.oneOfType([PropTypes.object]),
    dispatch: PropTypes.func.isRequired
};

export default withRouter(connect(mapStateToProps)(Client));

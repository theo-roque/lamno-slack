import { actions as clientActions } from "lsscreens/Client/Client.reducer";
import { push } from "react-router-redux";

export const signOut = domain => {
    return dispatch => {
        dispatch(signOutStarted());
        dispatch(push(`/${domain}`));
    };
};

export const reset = () => {
    return dispatch => dispatch(resetStarted());
};

const signOutStarted = () => ({
    type: clientActions.START_SIGNOUT
});

const resetStarted = () => ({
    type: clientActions.RESET_STATE
});

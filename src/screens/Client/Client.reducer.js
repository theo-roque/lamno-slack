export const actions = {
    START_SIGNOUT: "START_SIGNOUT",
    RESET_STATE: "RESET_STATE"
};

export const initialState = {};

export default (state = initialState) => {
    return state;
};
